/*
 * Copyright (c) 2024 Erik Ridderby, ARCHEA.
 * All rights reserved.
 *
 * This source code is licensed under both the 
 * * BSD-3-Clause license with No Nuclear or Weapons use exception
 *   (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in 
 *   the root directory of this source tree) 
 *
 * and the 
 * 
 * * GPLv3 
 *   (found in the COPYING file in the root directory of this source tree).
 * 
 * You may select, at your option, one of the above-listed licenses.
 * 
 */
 

#include "QProQuintUnitTests.h"

#include <QtTest>
#include "QProQuint/qproquint.h"
#include "proquint/proquint.h"

#include <QRandomGenerator>

#include "QtDebug"

void QProQuintUnitTests::basicValidation()
{
    QByteArray  cBuff(11, 0);
    quint16 a = 0b1000100100010010;

    uint2quint(cBuff.data(), a, '-');
    QVERIFY(QProQuint::toString(a) == QString::fromUtf8(cBuff).trimmed().remove('\x00').right(5));
    QVERIFY(QProQuint::uint16FromString( QProQuint::toString(a) ) == a);

    quint32 b = QRandomGenerator::global()->generate();

    uint2quint(cBuff.data(), b, '-');
    // qDebug() << Qt::bin << b << " => " << QProQuint::toString(b) << " original => " << QString::fromUtf8(cBuff).trimmed().remove('\x00');

    QVERIFY(QProQuint::toString(b) == QString::fromUtf8(cBuff).trimmed().remove('\x00'));
    QVERIFY(QProQuint::uint32FromString( QProQuint::toString(b) ) == b);

}

void QProQuintUnitTests::multipleRandomValidations()
{
    QByteArray  cBuff(11, 0);
    qDebug() << "Run 100000 random values and verify towards original implementation";
    for(int i = 0; i < 100000; i++)
    {
        quint32 random = QRandomGenerator::global()->generate();

        uint2quint(cBuff.data(), random, '-');
        QVERIFY(QProQuint::toString(random) == QString::fromUtf8(cBuff).trimmed().remove('\x00'));

        QVERIFY(QProQuint::uint32FromString( QProQuint::toString(random) ) == random);

    }
    qDebug() << "Random values done.";
}

void QProQuintUnitTests::toString16Grey()
{
    bool bOk = true;
    QVERIFY(QProQuint::uint16FromString("tatib", &bOk) == 0xd350);
    QVERIFY(true == bOk);
    QVERIFY(QProQuint::uint16FromString("(null)", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("xatib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tetib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tayib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tateb", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tatiq", &bOk) == 0x00);
    QVERIFY(false == bOk);


    // Wrong type of char on the wrong place.
    QVERIFY(QProQuint::uint16FromString("aatib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tmtib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("taaib", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tatjb", &bOk) == 0x00);
    QVERIFY(false == bOk);
    QVERIFY(QProQuint::uint16FromString("tatiu", &bOk) == 0x00);
    QVERIFY(false == bOk);

    // Not enough quints
    QVERIFY(QProQuint::uint16FromString("tati", &bOk) == 0x00);
    QVERIFY(false == bOk);

    // To many quints
    QVERIFY(QProQuint::uint16FromString("tatibi", &bOk) == 0x00);
    QVERIFY(false == bOk);

}

void QProQuintUnitTests::toString32Grey()
{
    bool bOk = true;
    QVERIFY(QProQuint::uint32FromString("suman-tatib", &bOk) == 0xce09d350);
    QVERIFY(true == bOk);

    // Not enough quints
    QVERIFY(QProQuint::uint32FromString("suman-tati", &bOk) == 0x00);
    QVERIFY(false == bOk);

    QVERIFY(QProQuint::uint32FromString("suma-tati", &bOk) == 0x00);
    QVERIFY(false == bOk);

    QVERIFY(QProQuint::uint32FromString("suma", &bOk) == 0x00);
    QVERIFY(false == bOk);

    // To many quints
    QVERIFY(QProQuint::uint32FromString("suman-tatibi", &bOk) == 0x00);
    QVERIFY(false == bOk);


}


