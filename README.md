# QProQuint Tests

Unittests for QProQuint. 

## Assumptions
The Build-system assumes that the source of QProQuint is 
either checked out under QProQuint/ or symbolically linked
there. 



    
# Copyright and License
Copyright (c) 2024 Erik Ridderby, ARCHEA.
All rights reserved.

This source code is licensed under both the 
* BSD-3-Clause license with No Nuclear or Weapons use exception (found in the BSD-3-Clause-No-Nuclear-or-Weapons-License.txt file in the root directory of this source tree) and the 
* GPLv3 (found in the COPYING file in the root directory of this source tree).

You may select, at your option, one of the above-listed licenses.
